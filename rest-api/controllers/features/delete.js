const prisma = require('../../db')

const SINGLE_CREATE = `
mutation($id: ID!) {
  instance: updateInstance(where: {id: $id}, data: { 
    isActive: false
  }) {
    id
  }
  observations: updateManyObservations(where: {
      parent: {id: $id}
    },
    data: {
      isActive: false
    }
  ) {
    count
  }
}
`

exports.deleteOne = (req, res, next) => {
  prisma
    .request(SINGLE_CREATE, {
      id: req.params.id,
    })
    .then(result => {
      console.log(result)
      return res.redirect(303, `/${req.model.url}/${result.data.instance.id}`)
    })
    .catch(next)
}
