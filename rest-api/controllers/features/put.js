const _ = require('lodash')
const {SINGLE_QUERY, FRAGMENT} = require('./queries')
const Promise = require('bluebird')
const update = require('immutability-helper')
const prisma = require('../../db')

const defaultAcc = {
  deletedObservationIds: [],
  createdObservations: [],
}

// There is a bug in prisma bindings so we need to make these sepatartely
const SINGLE_UPDATE = `
mutation(
  $instanceId: ID!,
  $name: String,
  $observations: [ObservationCreateWithoutParentInput!],
) {
  updateInstance(where: {id: $instanceId}, data: {
    name: $name,
    observations: {
      create: $observations
    }
  }) {
    id
    observations (where: {isActive: true}) {
      id
      createdAt
    }
  }
}
`

const DEACTIVATE = `
mutation($deletedObservationIds: [ID!]!) {
  updateManyObservations(where: {id_in: $deletedObservationIds}
    data: {isActive: false}
  ) {
    count
  }
}
`

exports.updateOne = (req, res, next) => {
  const attributes = _.get(req, 'model.attributes', [])
  const newObservations = _.get(req, 'body.data')

  const currentInstancePromise = prisma.request(SINGLE_QUERY, {
    ...req.params,
    model: req.model.id,
  })

  const currentObservationPromise = currentInstancePromise.then(instance => {
    return _.chain(instance)
      .get('data.instance.observations', [])
      .filter({isActive: true})
      .value()
  })

  return Promise.join(currentObservationPromise, currentInstancePromise).spread(
    (observations, {data}) => {
      const observationMutations = getObservationDeltas(
        attributes,
        newObservations,
        observations,
      )

      const {deletedObservationIds, createdObservations} = observationMutations

      const updatePromise = prisma.request(SINGLE_UPDATE, {
        instanceId: data.instance.id,
        name: req.body.name,
        observations: createdObservations,
      })

      const deactivatePromise = prisma.request(DEACTIVATE, {
        deletedObservationIds,
      })

      Promise.join(updatePromise, deactivatePromise)
        .then(result => {
          return res.redirect(303, `/${req.model.url}/${data.instance.id}`)
        })
        .catch(next)
    },
  )
}

function getObservationDeltas(attributes, newObservations, observations) {
  return _.reduce(
    attributes,
    (acc, attr) => {
      let oldObservatoin
      const newObservation = _.get(newObservations, attr.name)

      if (_.isUndefined(newObservation)) {
        return acc
      }

      if (attr.isArray) {
        oldObservation = _.filter(observations, o => o.attribute.id === attr.id)
      } else {
        oldObservation = _.find(observations, o => o.attribute.id === attr.id)
      }

      // Update with a value literal
      if (!attr.hasModel && !attr.isArray) {
        if (_.get(oldObservation, 'value') !== newObservation) {
          const deleteable = _.chain(observations)
            .filter(o => o.attribute.id === attr.id)
            .map('id')
            .value()

          return update(acc, {
            deletedObservationIds: {
              $push: deleteable,
            },
            createdObservations: {
              $push: [
                {
                  value: newObservation,
                  attribute: {
                    connect: {id: attr.id},
                  },
                },
              ],
            },
          })
        }
      }
      // Update with single instance
      else if (attr.hasModel && !attr.isArray) {
        const oldInstanceId = _.get(oldObservation, 'instance.id')
        if (oldInstanceId !== newObservation.id) {
          const deleteable = _.chain(observations)
            .filter(o => o.attribute.id === attr.id)
            .map('id')
            .value()

          return update(acc, {
            deletedObservationIds: {
              $push: deleteable,
            },
            createdObservations: {
              $push: [
                {
                  instance: {
                    connect: {id: newObservation.id},
                  },
                  attribute: {
                    connect: {id: attr.id},
                  },
                },
              ],
            },
          })
        }
      }
      // Update a list of values
      else if (!attr.hasModel && attr.isArray) {
        const oldValues = _.map(oldObservation, 'value')
        const deleteableObservations = _.chain(oldValues)
          .differenceWith(newObservation)
          .map(v => _.find(oldObservation, {value: v}))
          .map('id')
          .value()

        const createObservations = _.chain(newObservation)
          .differenceWith(oldValues)
          .map(v => ({attribute: {connect: {id: attr.id}}, value: v}))
          .value()

        return update(acc, {
          deletedObservationIds: {
            $push: deleteableObservations,
          },
          createdObservations: {
            $push: createObservations,
          },
        })
      }
      // UPdate list of models
      else if (attr.hasModel && attr.isArray) {
        const oldInstanceIds = _.map(oldObservation, 'instance.id')
        const newInstanceIds = _.map(newObservation, 'id')

        const deleteableObservations = _.chain(oldInstanceIds)
          .differenceWith(newInstanceIds)
          .map(i => _.filter(oldObservation, o => o.instance.id === i))
          .flatten()
          .map('id')
          .value()

        const createObservations = _.chain(newInstanceIds)
          .differenceWith(oldInstanceIds)
          .map(id => ({
            attribute: {connect: {id: attr.id}},
            instance: {connect: {id: id}},
          }))
          .value()

        return update(acc, {
          deletedObservationIds: {
            $push: deleteableObservations,
          },
          createdObservations: {
            $push: createObservations,
          },
        })
      }

      return acc
    },
    defaultAcc,
  )
}
