const _ = require('lodash')
const prisma = require('../../db')
const {MODEL_QUERY} = require('./queries')

const gets = require('./get')
const posts = require('./post')
const deletes = require('./delete')
const puts = require('./put')

const getModel = (req, res, next, model) => {
  prisma
    .request(MODEL_QUERY, {url: model})
    .then(result => {
      if (!_.get(result, 'data.model')) {
          next(new Error('404'))
      }
      req.model = _.get(result, 'data.model')
      return next()
    })
    .catch(next)
}

module.exports = _.merge({getModel}, gets, posts, deletes, puts)
