const prisma = require('../../db')
const update = require('immutability-helper')
const _ = require('lodash')
const {
  LIST_QUERY,
  SINGLE_QUERY,
  SINGLE_UPDATE,
  SINGLE_CREATE,
  SINGLE_DELETE,
} = require('./queries')

exports.findOne = (req, res, next) => {
  prisma
    .request(SINGLE_QUERY, req.params)
    .then(result => res.json(result))
    .catch(next)
}

exports.findAll = (req, res, next) => {
  prisma
    .request(LIST_QUERY, req.query)
    .then(result => res.json(result))
    .catch(next)
}

exports.updateOne = (req, res, next) => {
  prisma
    .request(SINGLE_UPDATE, {
      data: req.body,
      ...req.params,
    })
    .then(result => res.json(result))
    .catch(next)
}

exports.createOne = (req, res, next) => {
  const rawUrl = req.body.url || req.body.name
  const url = _.kebabCase(rawUrl)

  prisma
    .request(SINGLE_CREATE, {
      data: {...req.body, url},
    })
    .then(result => res.json(result))
    .catch(next)
}

exports.deleteOne = (req, res, next) => {
  if (!req.query.confirm) {
    return res.json({
      message: 'Are you sure?',
      warning: `Deleting this type could break your application.`,
    })
  }

  prisma
    .request(SINGLE_DELETE, req.params)
    .then(result => res.json(result))
    .catch(next)
}
