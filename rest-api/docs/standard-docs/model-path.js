const config = require('../config')

module.exports = {
  '/_models': {
    get: {
      tags: ['Models'],
      summary: 'A list of models',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'GET: Models',
      parameters: [
        {
          name: 'limit',
          in: 'query',
          required: false,
          description: 'The maximum number of models to be returned.',
          schema: {
            type: 'integer',
            default: 1000,
          },
        },
        {
          name: 'offset',
          in: 'query',
          required: false,
          description: 'The number of models to offset the result by.',
          schema: {
            type: 'integer',
            default: 0,
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  data: {
                    title: 'Data',
                    type: 'object',
                    properties: {
                      models: {
                        title: 'Models',
                        type: 'array',
                        items: {
                          $ref: '#/components/schemas/_model',
                        },
                      },
                      modelsConnection: {
                        $ref: '#/components/schemas/_connection',
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
    post: {
      tags: ['Models'],
      summary: 'Create a model',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      requestBody: {
        description: 'The request body',
        required: true,
        content: {
          'application/json': {
            schema: {
              title: 'Model Body',
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'My Amazing Experiment',
                  description: 'The experiments name',
                },
                url: {
                  type: 'string',
                  example: 'a-experiment',
                  description: 'The url part to access the model instances',
                },
                attributes: {
                  $ref: '#/components/schemas/_connect',
                },
              },
            },
          },
        },
      },
      operationId: 'POST: Model',
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  models: {
                    title: 'Models',
                    type: 'array',
                    items: {
                      $ref: '#/components/schemas/_model',
                    },
                  },
                  modelsConnection: {
                    $ref: '#/components/schemas/_connection',
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  '/_models/{id}': {
    get: {
      tags: ['Models'],
      summary: 'A single model',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'GET: Model',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the model to return',
          schema: {
            type: 'string',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  model: {
                    $ref: '#/components/schemas/_model',
                  },
                },
              },
            },
          },
        },
      },
    },
    put: {
      tags: ['Models'],
      summary: 'Update a single of model',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'PUT: Model',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the model to update',
          schema: {
            type: 'string',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
      ],
      requestBody: {
        description: 'The request body',
        required: true,
        content: {
          'application/json': {
            schema: {
              title: 'Model Body',
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'My Amazing Experiment',
                  description: 'The experiments name',
                },
                attributes: {
                  $ref: '#/components/schemas/_connectOrDisconnect',
                },
              },
            },
          },
        },
      },
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  model: {
                    $ref: '#/components/schemas/_model',
                  },
                },
              },
            },
          },
        },
      },
    },
    delete: {
      tags: ['Models'],
      summary: 'Delete a single model',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'DELETE: Model',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the model to delete',
          schema: {
            type: 'string',
            format: 'cuid',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
        {
          name: 'confirm',
          in: 'query',
          required: true,
          description:
            'Ensures that the user is aware of the implications of deleting a model',
          schema: {
            type: 'boolean',
            example: 'true',
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  data: {
                    title: 'data',
                    type: 'object',
                    properties: {
                      deleteModel: {
                        type: 'object',
                        properties: {
                          id: {
                            type: 'string',
                            format: 'cuid',
                            example: 'cjo55zor6000n0b246pjmiehb',
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
}
