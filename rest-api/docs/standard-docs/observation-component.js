module.exports = {
  _observation: {
    title: 'Observation',
    type: 'object',
    properties: {
      id: {
        type: 'string',
        format: 'cuid',
        example: 'cjo3j0ngc00470924auczhlu2',
      },
      value: {
        type: 'string',
        example: '23',
        description: 'Value of observation',
      },
      uri: {
        type: 'string',
        example: 'http://localhost:3005/_attributes/cjo3i8o9v002j0924hav8d1pq',
        description: 'A URI to get more information',
      },
      createdAt: {
        type: 'dateTime',
        example: '2018-11-12T15:15:00',
        description: 'Entry creation time',
      },
      updatedAt: {
        type: 'dateTime',
        example: '2018-11-12T15:15:00',
        description: 'Value update time',
      },
      deletedAt: {
        type: 'dateTime',
        example: '2018-11-12T15:15:00',
        description: 'Deletion time if not active',
      },
      isActive: {
        type: 'boolean',
        example: 'true',
        description: 'If current and not-deleted observation',
      },
      attribute: {
        type: 'object',
        description: 'The attribute this observation is recording',
        properties: {
          id: {
            type: 'string',
            format: 'cuid',
            example: 'cjo3j0ngc00470924auczhlu2',
          },
          name: {
            type: 'string',
            example: 'Height',
            description: 'Name of attribute',
          },
          uri: {
            type: 'string',
            example:
              'http://localhost:3005/_attributes/cjo3i8o9v002j0924hav8d1pq',
            description: 'A URI to get more information',
          },
          hasModel: {
            type: 'boolean',
            example: 'false',
            description: 'Is attribute associated with a model',
          },
          isArray: {
            type: 'boolean',
            example: 'false',
            description: 'Does attribute retain an array of observations',
          },
          type: {
            type: 'string',
            example: 'Attribute value type',
            enum: ['string', 'integer', 'float', 'boolean'],
            default: 'string',
          },
          model: {
            type: 'object',
            description:
              'The model-type this observation can record a connection to',
            properties: {
              id: {
                type: 'string',
                format: 'cuid',
                example: 'cjo3i8o9v002j0924hav8d1pq',
              },
              name: {
                type: 'string',
                example: 'Leaf',
              },
              url: {
                type: 'string',
                example: 'leaf',
                description: 'The Url part to access the model',
              },
              uri: {
                type: 'string',
                example:
                  'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
                description: 'A URI to get more information',
              },
            },
          },
        },
      },
      parent: {
        type: 'object',
        description: 'The model-instance this observation is recorded against',
        properties: {
          id: {
            type: 'string',
            format: 'cuid',
            example: 'cjo3j0ngc00470924auczhlu2',
          },
          name: {
            type: 'string',
            example: 'Plant 5',
            description: 'Name of instance',
          },
          uri: {
            type: 'string',
            example: 'http://localhost:3005/plant/cjo3i8o9v002j0924hav8d1pq',
            description: 'A URI to get more information',
          },
          model: {
            type: 'object',
            description:
              'The model-type of the instance this observation is recorded against',
            properties: {
              id: {
                type: 'string',
                format: 'cuid',
                example: 'cjo3i8o9v002j0924hav8d1pq',
              },
              name: {
                type: 'string',
                example: 'My Model',
              },
              url: {
                type: 'string',
                description: 'The Url part to access the model',
                example: 'plant',
              },
              uri: {
                type: 'string',
                example:
                  'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
                description: 'A URI to get more information',
              },
            },
          },
        },
      },

      instance: {
        type: 'object',
        description:
          'The model-instance this observation defines a parent->child relation to',
        properties: {
          id: {
            type: 'string',
            format: 'cuid',
            example: 'cjo3j0ngc00470924auczhlu2',
          },
          name: {
            type: 'string',
            example: 'Leaf 7',
            description: 'Name of instance',
          },
          uri: {
            type: 'string',
            example: 'http://localhost:3005/leaf/cjo3i8o9v002j0924hav8d1pq',
            description: 'A URI to get more information',
          },
          model: {
            type: 'object',
            description:
              'The model-type of the instance this observation defines a parent->child relation to',
            properties: {
              id: {
                type: 'string',
                format: 'cuid',
                example: 'cjo3i8o9v002j0924hav8d1pq',
              },
              name: {
                type: 'string',
                example: 'A Leaf',
              },
              url: {
                type: 'string',
                description: 'The Url part to access the model',
                example: 'leaf',
              },
              uri: {
                type: 'string',
                example:
                  'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
                description: 'A URI to get more information',
              },
            },
          },
        },
      },
    },
  },
}
