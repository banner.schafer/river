const express = require('express')
const router = express.Router()
const controller = require('../controllers/attributes')

router.get('/', controller.findAll)
router.get('/:id', controller.findOne)
router.put('/:id', controller.updateOne)
router.post('/', controller.createOne)
router.delete('/:id', controller.deleteOne)

module.exports = router
