const express = require('express')
const router = express.Router()
const controller = require('../controllers/features')
const prisma = require('../db')
const _ = require('lodash')

const QUERY = `
  query($id: ID!) {
    node(id: $id) {
      id
      __typename
    }
    instance (where: {id: $id}) {
      id
      __typename
      model {
        id
        url
      }
    }
  }
`

router.get('/:id', (req, res, next) => {
  let url = `${req.protocol}://${req.get('host')}`
  prisma
    .request(QUERY, req.params)
    .then(result => {
      const type = _.get(result, 'data.node.__typename')
      const id = _.get(result, 'data.node.id')
      const instanceUrl = _.get(result, 'data.instance.model.url')

      switch (type) {
        case 'Model':
          url += `/_models/${id}`
          break
        case 'Attribute':
          url += `/_attributes/${id}`
          break
        case 'Observation':
          url += `/_observations/${id}`
          break
        case 'Instance':
          url += `/${instanceUrl}/${id}`
          break
        default:
          return res.status(404).json({message: 'That does not exist'})
      }

      if (req.headers.redirect === 'false') {
        return res.json({
          url: url,
          type: type,
          id: id,
        })
      } else {
        return res.redirect(url)
      }
    })
    .catch(next)
})

module.exports = router
