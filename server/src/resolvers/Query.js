const {forwardTo} = require('prisma-binding')

const Query = {
  instances: forwardTo('db'),
  instance: forwardTo('db'),
  observations: forwardTo('db'),
  observation: forwardTo('db'),
  models: forwardTo('db'),
  model: forwardTo('db'),
  attributes: forwardTo('db'),
  attribute: forwardTo('db'),
  node: forwardTo('db'),
}

module.exports = {Query}
