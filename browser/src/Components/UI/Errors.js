import React from 'react'
import Typography from '@material-ui/core/Typography'
import {map} from 'lodash'

const Errors = props => {
  if (props.error.graphQLErrors) {
    return (
      <React.Fragment>
        <Typography color="error" variant="title">
          Error
        </Typography>
        <ul>
          {map(props.error.graphQLErrors, (error, index) => {
            return (
              <li key={index}>
                <Typography>{error.message}</Typography>
              </li>
            )
          })}
        </ul>
      </React.Fragment>
    )
  }
  return (
    <React.Fragment>
      <Typography color="error" variant="title">
        Error
      </Typography>
      <Typography component="pre">
        {JSON.stringify(props.error, null, 4)}
      </Typography>
    </React.Fragment>
  )
}

export default Errors
