import React, {Component} from 'react'
import Button from '@material-ui/core/Button'
import {withStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Searcher from './Searcher'
import TextField from '@material-ui/core/TextField'

import {concat, chain, reduce} from 'lodash'
import update from 'immutability-helper'

const styles = theme => ({
  buttons: {
    display: 'flex',
  },
  content: {
    paddingLeft: theme.spacing.unit * 2,
  },
  root: {
    marginTop: theme.spacing.unit * 1,
    marginBottom: theme.spacing.unit * 2,
  },
})

class ConnectOrCreate extends Component {
  constructor(props) {
    super(props)

    const form = reduce(
      props.attribute.model.attributes,
      (acc, field) => {
        acc[field.id] = ''

        return acc
      },
      {},
    )

    this.state = {createOrConnect: 'connectId', form: form}
  }

  onChange = value => {
    if (
      this.state.createOrConnect === 'connect' ||
      this.state.createOrConnect === 'connectId'
    ) {
      this.props.onChange({
        attribute: {connect: {id: this.props.attribute.id}},
        instance: {connect: {id: value.id}},
      })
    }
  }

  updateField = fieldId => e => {
    const value = e.target.value
    const newState = update(this.state, {
      form: {[fieldId]: {$set: value}},
    })
    this.setState(newState)

    const observations = reduce(
      newState.form,
      (acc, value, attrId) => {
        if (!value) {
          return acc
        } else {
          const obs = {
            attribute: {connect: {id: attrId}},
            value: value,
          }

          return concat(acc, [obs])
        }
      },
      [],
    )

    const nameAttr = chain(this.props.attribute)
      .get('model.attributes')
      .find({name: 'name'})
      .get('id')
      .value()

    const name = newState.form[nameAttr]

    this.props.onChange({
      attribute: {connect: {id: this.props.attribute.id}},
      instance: {
        create: {
          model: {connect: {id: this.props.attribute.model.id}},
          observations: {create: observations},
          name: name,
        },
      },
    })
  }

  render() {
    const {classes} = this.props

    return (
      <div className={classes.root}>
        <Typography variant="subheading" gutterBottom>
          {this.props.attribute.name}:
        </Typography>
        <div className={classes.content}>
          <div className={classes.buttons}>
            <Button
              fullWidth
              variant={
                this.state.createOrConnect === 'connectId'
                  ? 'raised'
                  : 'outlined'
              }
              onClick={() => {
                this.setState({createOrConnect: 'connectId'})
              }}
              color={
                this.state.createOrConnect === 'connectId'
                  ? 'secondary'
                  : 'default'
              }>
              Connect By Id
            </Button>
            <Button
              fullWidth
              variant={
                this.state.createOrConnect === 'connect' ? 'raised' : 'outlined'
              }
              onClick={() => {
                this.setState({createOrConnect: 'connect'})
              }}
              color={
                this.state.createOrConnect === 'connect'
                  ? 'secondary'
                  : 'default'
              }>
              Connect By Name
            </Button>
            <Button
              fullWidth
              variant={
                this.state.createOrConnect === 'create' ? 'raised' : 'outlined'
              }
              onClick={() => {
                this.setState({createOrConnect: 'create'})
              }}
              color={
                this.state.createOrConnect === 'create'
                  ? 'secondary'
                  : 'default'
              }>
              Create
            </Button>
          </div>

          {this.state.createOrConnect === 'connect' && (
            <Searcher
              label={this.props.attribute.name}
              query={this.props.searchTerms}
              onChange={this.onChange}
            />
          )}

          {this.state.createOrConnect === 'connectId' && (
            <Searcher
              label={this.props.attribute.name}
              query={this.props.idSearchTerms}
              onChange={this.onChange}
            />
          )}

          {this.state.createOrConnect === 'create' &&
            chain(this.props.attribute.model.attributes)
              .filter({isArray: false, hasModel: false})
              .map(field => (
                <TextField
                  key={field.id}
                  label={field.name}
                  fullWidth
                  variant="outlined"
                  margin="normal"
                  onChange={this.updateField(field.id)}
                />
              ))
              .value()}
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(ConnectOrCreate)
