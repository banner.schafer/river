import React, {Component} from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Ontologies from './Containers/Models/Router'
import Instance from './Containers/Instances/Router'
import Home from './Components/Home'
import Viz from './Containers/GraphViz/Router'
import Resolver from './Containers/IdResolver/Router'
import Observations from './Containers/Observations/Router'
import Attributes from './Containers/Attributes/Router'

import Nav from './Components/Nav/Nav'

class App extends Component {
  render() {
    return (
      <Router>
        <Nav>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/models" component={Ontologies} />
            <Route path="/instance" component={Instance} />
            <Route path="/graph-viz" component={Viz} />
            <Route path="/resolver" component={Resolver} />
            <Route path="/observations" component={Observations} />
            <Route path="/attributes" component={Attributes} />
          </Switch>
        </Nav>
      </Router>
    )
  }
}

export default App
