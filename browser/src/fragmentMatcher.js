import {IntrospectionFragmentMatcher} from 'apollo-cache-inmemory'

const fm = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    __schema: {
      types: [
        {
          kind: 'INTERFACE',
          name: 'Node',
          possibleTypes: [
            {
              name: 'Instance',
            },
            {
              name: 'Observation',
            },
            {
              name: 'Attribute',
            },
            {
              name: 'Model',
            },
          ],
        },
      ],
    },
  },
})

export default fm
