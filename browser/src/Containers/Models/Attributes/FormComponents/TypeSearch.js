import React from 'react'

import SearchType from '../../Search/Type'
import {ValidatorForm} from 'react-material-ui-form-validator'

import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'

import {withStyles} from '@material-ui/core/styles'

const styles = theme => ({
  contentPadding: {
    padding: theme.spacing.unit * 2,
  },
  gutterBottom: {
    paddingBottom: theme.spacing.unit * 4,
  },
  divider: {
    marginTop: '25px',
    marginBottom: '25px',
  },
})

const TypeSearch = props => {
  const {classes} = props
  return (
    <ValidatorForm onSubmit={props.submit}>
      <SearchType onChange={props.updateType} />
      <Divider className={classes.divider} />
      <div style={{float: 'right'}}>
        <Button onClick={props.back}>Back</Button>
        <Button type="submit" variant="contained" color="primary">
          Next
        </Button>
      </div>
    </ValidatorForm>
  )
}

export default withStyles(styles)(TypeSearch)
