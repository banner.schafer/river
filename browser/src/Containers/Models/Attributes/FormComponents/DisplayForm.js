import React from 'react'

import {ValidatorForm} from 'react-material-ui-form-validator'
import {withStyles} from '@material-ui/core/styles'

import FormLabel from '@material-ui/core/FormLabel'
import Tooltip from '@material-ui/core/Tooltip'
import Switch from '@material-ui/core/Switch'
import Help from '@material-ui/icons/Help'
import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'

const styles = theme => ({
  contentPadding: {
    padding: theme.spacing.unit * 2,
  },
  gutterBottom: {
    paddingBottom: theme.spacing.unit * 4,
  },
  divider: {
    marginTop: '25px',
    marginBottom: '25px',
  },
})

const DisplayForm = props => {
  const {phenomenon, classes} = props
  return (
    <ValidatorForm onSubmit={props.submit}>
      <FormLabel component="legend">
        Is list?
        <Tooltip
          placement="right"
          title="Is this phenomenon a list of observations or just a single observation">
          <Help />
        </Tooltip>
      </FormLabel>
      <Switch checked={phenomenon.isArray} onChange={props.updateIsArray} />
      <Divider className={classes.divider} />
      <div style={{float: 'right'}}>
        <Button onClick={props.back}>Back</Button>
        <Button type="submit" variant="contained" color="primary">
          Finish
        </Button>
      </div>
    </ValidatorForm>
  )
}

export default withStyles(styles)(DisplayForm)
