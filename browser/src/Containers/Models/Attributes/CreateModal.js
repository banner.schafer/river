import React, {Component} from 'react'
import Modal from '../../../Components/UI/MinimalModal'

import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import CircularProgress from '@material-ui/core/CircularProgress'
import Error from '../../../Components/UI/Errors'

import CreatePhenomenon from './Create'

import {graphql} from 'react-apollo'
import gql from 'graphql-tag'
import {listFragment} from './fragments'

const MUTATION = gql`
  mutation createAttribute($input: AttributeCreateInput!) {
    createAttribute(data: $input) {
      ...listAttributes
    }
  }
  ${listFragment}
`

const QUERY = gql`
  query models($id: ID!) {
    model(where: {id: $id}) {
      id
      attributes {
        ...listAttributes
      }
    }
  }
  ${listFragment}
`

class Create extends Component {
  state = {
    loading: false,
    error: null,
  }

  submit = phenomenon => {
    this.setState({loading: true, error: false})
    phenomenon.parentModels = {connect: {id: this.props.type.id}}

    if (phenomenon.hasModel) {
      phenomenon.model = {connect: {id: phenomenon.type.id}}
      delete phenomenon.type
    }

    this.props
      .mutate({variables: {input: phenomenon}})
      .then(() => {
        this.setState({loading: false})
        this.props.closeModal()
      })
      .catch(err => {
        this.setState({loading: false, error: err})
      })
  }

  render() {
    return (
      <Modal handleClose={this.props.closeModal} open={this.props.open}>
        {this.state.error && <Error error={this.state.error} />}
        <Typography gutterBottom variant="headline">
          Create Phenomenon
          {this.state.loading && (
            <CircularProgress color="secondary" size={25} />
          )}
        </Typography>
        <Divider />
        <CreatePhenomenon submit={this.submit} />
      </Modal>
    )
  }
}

export default graphql(MUTATION, {
  options: {
    update: (proxy, {data: {createAttribute}}) => {
      const data = proxy.readQuery({
        query: QUERY,
        variables: {id: createAttribute.parentModels[0].id},
      })
      data.model.attributes.push(createAttribute)
      proxy.writeQuery({query: QUERY, data: data})
    },
  },
})(Create)
