import React, {Component} from 'react'
import {Switch, Route} from 'react-router-dom'

import List from './List'
import Update from './Update'

class FeatureRouter extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={this.props.match.path} component={List} />
        <Route exact path={`${this.props.match.path}/:id`} component={Update} />
      </Switch>
    )
  }
}

FeatureRouter.propTypes = {}

export default FeatureRouter
