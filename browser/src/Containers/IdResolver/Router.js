import React, {Component} from 'react'
import {Switch, Route} from 'react-router-dom'

import Resolver from './Resolver'

class Router extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={`${this.props.match.path}`} component={Resolver} />
      </Switch>
    )
  }
}

export default Router
