import React, {Component} from 'react'

import LinearProgress from '@material-ui/core/LinearProgress'
import Error from '../../Components/UI/Errors'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Paper from '@material-ui/core/Paper'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import {withStyles} from '@material-ui/core/styles'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import Delete from '@material-ui/icons/Delete'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'

import {map} from 'lodash'
import gql from 'graphql-tag'
import {graphql, compose} from 'react-apollo'
import {Link} from 'react-router-dom'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
  },
  title: {
    display: 'inline-flex',
    width: '100%',
  },
})

const QUERY = gql`
  query($url: String!) {
    instances(where: {isActive: true, model: {url: $url}}) {
      id
      name
    }
    model(where: {url: $url}) {
      id
      name
    }
  }
`

const MUTATION = gql`
  mutation($id: ID!) {
    deleteInstance(where: {id: $id}) {
      id
      isActive
      model {
        url
      }
    }
  }
`

class ListInst extends Component {
  render() {
    const {loading, error, instances, model} = this.props.data
    const {classes} = this.props

    if (loading) {
      return <LinearProgress color="secondary" />
    }

    if (error) {
      console.log(error)
      return <Error error={error} />
    }

    return (
      <Paper className={classes.root}>
        <div className={classes.title}>
          <Typography variant="headline" style={{flex: 1}}>
            {model.name}
          </Typography>
          <Button
            color="secondary"
            component={Link}
            to={`${this.props.location.pathname}/create`}>
            Create
          </Button>
        </div>
        <List>
          {map(instances, instance => {
            return (
              <ListItem
                button
                key={instance.id}
                onClick={() => {
                  this.props.history.push(
                    `${this.props.location.pathname}/${instance.id}`,
                  )
                }}>
                <Avatar>{instance.name[0]}</Avatar>
                <ListItemText primary={instance.name} />
                <ListItemSecondaryAction>
                  <IconButton
                    aria-label="delete"
                    onClick={() => {
                      this.props.mutate({variables: {id: instance.id}})
                    }}>
                    <Delete />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            )
          })}
        </List>
      </Paper>
    )
  }
}

export default compose(
  graphql(QUERY, {
    options: props => {
      return {
        pollInterval: 1000,
        variables: {
          url: props.match.params.url,
        },
      }
    },
  }),
  graphql(MUTATION, {
    options: {
      update: (proxy, {data: {deleteInstance}}) => {
        const QUERY = gql`
          query($url: String!) {
            instances(where: {isActive: true, model: {url: ${
              deleteInstance.model.url
            }}}) {
              id
              name
            }
          }
        `
        const data = proxy.readQuery({
          query: QUERY,
        })

        data.instances = data.instances.filter(i => {
          return i.id !== deleteInstance.id
        })

        proxy.writeQuery({
          query: QUERY,
          data: data,
        })
      },
    },
  }),
)(withStyles(styles)(ListInst))
