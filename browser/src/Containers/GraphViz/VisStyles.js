import React, { Component } from "react";
import { map, chain, concat } from "lodash";
import {
  InteractiveForceGraph,
  ForceGraphNode,
  ForceGraphLink
} from "react-vis-force";

import { withStyles } from "@material-ui/core/styles";

const styles = theme => {
  return {
    root: {
      padding: theme.spacing.unit * 2
    },
    dataComp: {
      backgroundColor: theme.palette.grey[50],
      borderRadius: theme.shape.borderRadius
    }
  };
};

class DrawComp extends Component {
  processData = () => {
    const modelNodes = map(this.props.models, model => {
      return {
        id: model.id,
        label: model.name,
        group: "blue"
      };
    });

    const attributeNodes = chain(this.props.attributes)
      .filter({
        hasModel: false
      })
      .map(attribute => {
        return { id: attribute.id, label: attribute.name, group: "red" };
      })
      .value();

    const nodes = concat(modelNodes, attributeNodes);

    const links = chain(this.props.attributes)
      .map(attribute => {
        return map(attribute.parentModels, parentModel => {
          return {
            source: parentModel.id,
            target: attribute.hasModel ? attribute.model.id : attribute.id,
            value: 1
          };
        });
      })
      .flatten()
      .value();

    return { nodes, links };
  };

  render() {
    const { nodes, links } = this.processData();
    const { classes } = this.props;

    return (
      <div className={classes.dataComp}>
        <InteractiveForceGraph
          simulationOptions={{ height: 900, width: 900, animate: true }}
          zoom
          showLabels
          labelAttr="label"
          highlightDependencies
        >
          {map(nodes, node => (
            <ForceGraphNode key={node.id} node={node} fill={node.group} />
          ))}
          {map(links, (link, index) => (
            <ForceGraphLink key={`${index}`} link={link} />
          ))}
        </InteractiveForceGraph>
      </div>
    );
  }
}

export default withStyles(styles)(DrawComp);
